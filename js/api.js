var response = [];

document.querySelector('#place').addEventListener('blur', () => {
    var place = document.querySelector('#place').value;
    if (place != "") {
        fetch("https://weatherapi-com.p.rapidapi.com/forecast.json?q=" + place + "&days=3&lang=pt", {
            "method": "GET",
            "headers": {
                "x-rapidapi-host": "weatherapi-com.p.rapidapi.com",
                "x-rapidapi-key": "182376fa68msh3dada38fcca660ap1744c2jsne60c8b6bdd78"
            }
        })
            .then(res => res.json())

            .then(data => {
                document.querySelector('#img').src = data.current.condition.icon;
                document.querySelector('#temp').innerHTML = data.current.temp_c + "ºC"
                document.querySelector('#pressure').innerHTML = "Precipitação: " + data.current.precip_mm + "mm"
                document.querySelector('#humidity').innerHTML = "Humidade: " + data.current.humidity + "%"
                document.querySelector('#wind').innerHTML = "Vento: " + data.current.wind_kph + "km/h"
                document.querySelector('#date').innerHTML = formatDate(data.forecast.forecastday[0].date)
                document.querySelector('#city').innerHTML = data.location.name + ", " + data.location.region + ", " + data.location.country
                document.querySelector('#description').innerHTML = "Neste momento: " + data.current.condition.text

                generateForecast(data);
            })
            .catch(err => console.error(err));

    }
});

function generateForecast(data) {
    var forecastCard = document.querySelector('#forecast');
    forecastCard.innerHTML = '';

    for (let i = 0; i < data.forecast.forecastday.length; i++) {
        let forecastContainer = createElement('div');
        let containerImg = createElement('div');
        let containerTemp = createElement('div');
        let container = createElement('div');

        let img = createElement('img');
        let date = createElement('span');
        let maxtemp = createElement('span');
        let mintemp = createElement('span');
        let dia = createElement('span');

        date.innerHTML = (i == 0) ? 'Hoje' : formatDate(data.forecast.forecastday[i].date);
        img.src = data.forecast.forecastday[i].day.condition.icon;
        mintemp.innerHTML = (data.forecast.forecastday[i].day.mintemp_c).toFixed(1) + 'ºC';
        maxtemp.innerHTML = (data.forecast.forecastday[i].day.maxtemp_c).toFixed(1) + 'ºC';
        dia.innerHTML = diaDaSemana(data.forecast.forecastday[i].date);

        
        forecastContainer.classList.add('card');
        forecastContainer.classList.add('shadow');
        forecastContainer.classList.add('col-2');
        forecastContainer.classList.add('mt-5');
        forecastContainer.classList.add('me-2');
        
        container.classList.add('row');
        
        img.classList.add('img-fluid');
        
        dia.classList.add('card-title');
        dia.classList.add('text-center');
        
        mintemp.classList.add('badge');
        mintemp.classList.add((data.forecast.forecastday[i].day.condition.text).toLowerCase().includes('chuva') ? 'bg-danger' : 'bg-primary');
        maxtemp.classList.add('badge');
        maxtemp.classList.add((data.forecast.forecastday[i].day.condition.text).toLowerCase().includes('chuva') ? 'bg-danger' : 'bg-primary');
        
        containerImg.classList.add('col');
        containerTemp.classList.add('col');
        containerTemp.classList.add('ms-4');
        
        containerImg.appendChild(img);
        containerTemp.appendChild(maxtemp)
        containerTemp.appendChild(mintemp)
       
        container.appendChild(containerImg);
        container.appendChild(containerTemp);

        forecastContainer.appendChild(container);
        forecastContainer.appendChild(dia);

        forecastCard.appendChild(forecastContainer);
    }
}

function formatDate(date) {
    return String(date).split('-').reverse().join('-');
}

function convert(temp, tipo) {
    switch (tipo) {
        case 'fahrenheint':
            return (temp * 9 / 5) + 32;
            break;
        case 'celsius':
            return (temp - 32) * 5 / 9;
            break;
        default:
            return 0;
    }
}

function createElement(el) {
    return document.createElement(el);
}

function diaDaSemana(data) {
    const semana = ["Domingo", "Segunda-Feira", "Terça-Feira", "Quarta-Feira", "Quinta-Feira", "Sexta-Feira", "Sábado"];

    var arr = data.split("-").reverse();
    var date = new Date(arr[0], arr[1] - 1, arr[2]);
    var dia = date.getDay();
    return semana[dia];
};